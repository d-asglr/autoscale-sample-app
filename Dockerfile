FROM mingc/android-build-box:1.21.1

# Support packages
ENV RAKE_VERSION=13.0.3
ENV BUNDLER_VERSION=2.1.4

# Setup locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen && export LC_ALL=en_US.UTF-8
